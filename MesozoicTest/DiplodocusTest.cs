﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class DiplodocusTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Diplodocus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }
        [TestMethod]
        public void TestRoar()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);

            Assert.AreEqual("*Diplodocus roar*", louis.roar());
        }
        [TestMethod]
        public void TestSayHello()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);

            Assert.AreEqual("Je suis Louis le Diplodocus, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestHug()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            Diplodocus regis = new Diplodocus("Regis", 27);

            Assert.AreEqual("Je suis Louis et je fais un calin a Regis.", louis.hug(regis));
        }
    }
}
