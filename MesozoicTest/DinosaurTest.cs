﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class DinosaurTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }
        [TestMethod]
        public void TestDinosaurRoar()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("*Stegausaurus roar*", louis.roar());
        }
        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestDinosaurGetName()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
        }
        [TestMethod]
        public void TestDinosaurGetSpecie()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Stegausaurus", louis.getSpecie());
        }
        [TestMethod]
        public void TestDinosaurGetAge()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual(12, louis.getAge());
        }
        [TestMethod]
        public void TestDinosaurHug()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            TyrannosaurusRex regis = new TyrannosaurusRex("Regis", 27);

            Assert.AreEqual("Je suis Louis et je fais un calin a Regis.", louis.hug(regis));
        }
        [TestMethod]
        public void TestToString()
        {
            Dinosaur dinosaur = new Stegausaurus("Louis", 12);
            Assert.AreEqual("Mesozoic.Stegausaurus = {name: Louis, age: 12}", dinosaur.ToString());
        }
        [TestMethod]
        public void TestEquals()
        {
            Dinosaur louis = new Stegausaurus("Louis", 12);
            Dinosaur roland = new Stegausaurus("Roland", 12);
            Dinosaur didier = new Diplodocus("Didier", 7);
            roland = louis;
            Assert.AreEqual(true, louis == roland);
            Assert.AreEqual(false, louis == didier);
        }
        [TestMethod]
        public void TestNotEquals()
        {
            Dinosaur louis = new Stegausaurus("Louis", 12);
            Dinosaur roland = new Stegausaurus("Roland", 12);
            Assert.AreEqual(true, louis != roland);
            Assert.AreEqual(false, louis == roland);
        }
    }
}
