﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class DinosaursTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            TyrannosaurusRex regis = new TyrannosaurusRex("Regis", 7);
            Stegausaurus jean = new Stegausaurus("Jean", 17);
            Triceratops philip = new Triceratops("Philip", 2);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Regis", regis.getName());
            Assert.AreEqual("Jean", jean.getName());
            Assert.AreEqual("Philip", philip.getName());
            Assert.AreEqual("Diplodocus", louis.getSpecie());
            Assert.AreEqual("TyrannosaurusRex", regis.getSpecie());
            Assert.AreEqual("Stegausaurus", jean.getSpecie());
            Assert.AreEqual("Triceratops", philip.getSpecie());
            Assert.AreEqual(12, louis.getAge());
            Assert.AreEqual(7, regis.getAge());
            Assert.AreEqual(17, jean.getAge());
            Assert.AreEqual(2, philip.getAge());
        }
        [TestMethod]
        public void TestRoar()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            TyrannosaurusRex regis = new TyrannosaurusRex("Regis", 7);
            Stegausaurus jean = new Stegausaurus("Jean", 17);
            Triceratops philip = new Triceratops("Philip", 2);

            Assert.AreEqual("*Diplodocus roar*", louis.roar());
            Assert.AreEqual("*TyrannosaurusRex roar*", regis.roar());
            Assert.AreEqual("*Stegausaurus roar*", jean.roar());
            Assert.AreEqual("*Triceratops roar*", philip.roar());
        }
        [TestMethod]
        public void TestSayHello()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            TyrannosaurusRex regis = new TyrannosaurusRex("Regis", 7);
            Stegausaurus jean = new Stegausaurus("Jean", 17);
            Triceratops philip = new Triceratops("Philip", 2);

            Assert.AreEqual("Je suis Louis le Diplodocus, j'ai 12 ans.", louis.sayHello());
            Assert.AreEqual("Je suis Regis le TyrannosaurusRex, j'ai 7 ans.", regis.sayHello());
            Assert.AreEqual("Je suis Jean le Stegausaurus, j'ai 17 ans.", jean.sayHello());
            Assert.AreEqual("Je suis Philip le Triceratops, j'ai 2 ans.", philip.sayHello());
        }
        [TestMethod]
        public void TestHug()
        {
            Diplodocus louis = new Diplodocus("Louis", 12);
            TyrannosaurusRex regis = new TyrannosaurusRex("Regis", 7);
            Stegausaurus jean = new Stegausaurus("Jean", 17);
            Triceratops philip = new Triceratops("Philip", 2);

            Assert.AreEqual("Je suis Louis et je fais un calin a Regis.", louis.hug(regis));
            Assert.AreEqual("Je suis Regis et je fais un calin a Jean.", regis.hug(jean));
            Assert.AreEqual("Je suis Jean et je fais un calin a Philip.", jean.hug(philip));
            Assert.AreEqual("Je suis Philip et je fais un calin a Louis.", philip.hug(louis));
        }
    }
}
