﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class TriceratopsTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            Triceratops louis = new Triceratops("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Triceratops", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }
        [TestMethod]
        public void TestRoar()
        {
            Triceratops louis = new Triceratops("Louis", 12);

            Assert.AreEqual("*Triceratops roar*", louis.roar());
        }
        [TestMethod]
        public void TestSayHello()
        {
            Triceratops louis = new Triceratops("Louis", 12);

            Assert.AreEqual("Je suis Louis le Triceratops, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestHug()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Triceratops regis = new Triceratops("Regis", 27);

            Assert.AreEqual("Je suis Louis et je fais un calin a Regis.", louis.hug(regis));
        }
    }
}
