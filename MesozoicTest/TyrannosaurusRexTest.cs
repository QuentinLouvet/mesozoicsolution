﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class TyrannosaurusRexTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("TyrannosaurusRex", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }
        [TestMethod]
        public void TestRoar()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);

            Assert.AreEqual("*TyrannosaurusRex roar*", louis.roar());
        }
        [TestMethod]
        public void TestSayHello()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);

            Assert.AreEqual("Je suis Louis le TyrannosaurusRex, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestHug()
        {
            TyrannosaurusRex louis = new TyrannosaurusRex("Louis", 12);
            TyrannosaurusRex regis = new TyrannosaurusRex("Regis", 27);

            Assert.AreEqual("Je suis Louis et je fais un calin a Regis.", louis.hug(regis));
        }
    }
}
