﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class LaboratoireTest
    {
        [TestMethod]
        public void TestTDinosaur()
        {
            Diplodocus Louis = new Diplodocus("Louis", 11);
            Dinosaur labLouis = Laboratoire.CreateDinosaur<Diplodocus>("Louis", 11);
            Assert.AreEqual(Louis.sayHello(), labLouis.sayHello());
        }
    }
}
