﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class HordeTest
    {
        [TestMethod]
        public void TestsayHelloHorde()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            TyrannosaurusRex regis = new TyrannosaurusRex("Regis", 27);
            Horde horde = new Horde("horde");
            horde.addToHorde(louis);
            horde.addToHorde(regis);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans. Je suis Regis le TyrannosaurusRex, j'ai 27 ans. ", horde.sayHello());
        }
    }
}
