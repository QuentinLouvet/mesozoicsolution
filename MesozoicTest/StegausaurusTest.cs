﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class StegausaurusTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }
        [TestMethod]
        public void TestRoar()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("*Stegausaurus roar*", louis.roar());
        }
        [TestMethod]
        public void TestSayHello()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestHug()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Stegausaurus regis = new Stegausaurus("Regis", 27);

            Assert.AreEqual("Je suis Louis et je fais un calin a Regis.", louis.hug(regis));
        }
    }
}
