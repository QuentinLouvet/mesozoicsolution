﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class TyrannosaurusRex : Dinosaur
    {
        protected override string Specie { get { return "TyrannosaurusRex"; } }

        public TyrannosaurusRex(string name, int age) : base(name, age) { }

        public override string roar()
        {
            return "*TyrannosaurusRex roar*";
        }
    }
}
