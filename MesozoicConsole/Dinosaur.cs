﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public abstract class Dinosaur
    {
        protected string name;
        protected virtual string Specie { get { return "Dinosaur"; } }
        protected int age;

        public Dinosaur(string name, int age)
        {
            this.name = name;
            this.age = age;
        }
        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.Specie, this.age);
        }
        public virtual string roar()
        {
            return "Grrr";
        }
        public string getName()
        {
            return this.name;
        }
        
        public string getSpecie()
        {
            return this.Specie;
        }
        
        public int getAge()
        {
            return this.age;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public void setAge(int age)
        {
            this.age = age;
        }
        public static NDinosaur Hug<NDinosaur>(Dinosaur dinosaur) where NDinosaur : Dinosaur
        {
            return (NDinosaur)Activator.CreateInstance();
        }
        public string hug(Dinosaur dinosaur)
        {
            return string.Format("Je suis {0} et je fais un calin a {1}.", this.name, dinosaur.name);
        }
        public override string ToString()
        {
            return base.ToString() + " = {name: " + name.ToString() + ", age: " + age.ToString() + "}";
        }
        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.name.GetHashCode();
            hash ^= this.age.GetHashCode();
            hash ^= this.Specie.GetHashCode();
            return hash;
        }
        public static bool operator ==(Dinosaur dino1, Dinosaur dino2)
        {
            return dino1.name == dino2.name && dino1.Specie == dino2.Specie && dino1.age == dino2.age;
        }
        public static bool operator !=(Dinosaur dino1, Dinosaur dino2)
        {
            return !(dino1 == dino2);
        }
        public override bool Equals(object obj)
        {
            return obj is Dinosaur && this == (Dinosaur)obj;
        }
    }
}
