﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class Horde
    {
        private string name;
        List<Dinosaur> horde = new List<Dinosaur>();

        public Horde(string name)
        {
            this.name = name;
        }
        public void addToHorde(Dinosaur dino)
        {
            horde.Add(dino);
        }
        public void removeFromHorde(Dinosaur dino)
        {
            horde.Remove(dino);
        }
        public string sayHello()
        {
            StringBuilder sb = new StringBuilder(5000);
            foreach(Dinosaur dino in horde)
            {
                sb.AppendFormat("{0} ",dino.sayHello());
            }
            return sb.ToString();
        }
    }
}
