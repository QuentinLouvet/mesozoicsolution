﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace MesozoicConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis = Laboratoire.CreateDinosaur<Diplodocus>("Louis", 11);
            Console.WriteLine(louis.sayHello());
            Console.ReadKey();
        }
    }
}
