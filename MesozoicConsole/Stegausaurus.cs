﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class Stegausaurus : Dinosaur
    {
        protected override string Specie { get { return "Stegausaurus"; } }

        public Stegausaurus(string name, int age) : base(name, age) { }
        public override string roar()
        {
            return "*Stegausaurus roar*";
        }
    }
}
