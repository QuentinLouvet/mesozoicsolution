﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class Triceratops : Dinosaur
    {
        protected override string Specie { get { return "Triceratops"; } }

        public Triceratops(string name, int age) : base(name, age) { }

        public override string roar()
        {
            return "*Triceratops roar*";
        }
    }
}
