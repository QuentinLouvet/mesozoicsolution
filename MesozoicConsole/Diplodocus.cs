﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class Diplodocus : Dinosaur
    {
        protected override string Specie { get { return "Diplodocus"; } }

        public Diplodocus(string name, int age) : base(name, age) { }

        public override string roar()
        {
            return "*Diplodocus roar*";
        }
    }
}
